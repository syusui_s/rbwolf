# rbwolf - Werewolf (Mafia) Game Bot
rbwolf is the bot for Werewolf (Mafia) Game.
It supports several chat services such as IRC and Slack.
This implementation is very similar to [phpwolf](https://github.com/gluxon/phpwolf).


## Installation

    $ gem install rbwolf

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( <https://bitbucket.org/syusui_s/rbwolf/fork> )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
